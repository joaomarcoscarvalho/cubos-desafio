<h1 align="center">
   👨🏿‍ Aplicativo de Filmes 
</h1>
  <p align="center">Neste aplicativo é possivel acessar dados como: avaliação, detalhe, trailer, descrição, imagem, data de lançamento, orçamento, lucro, gastos, idioma original e muito mais.</p>
</br>

  <p align="center">
  <img src="https://img.shields.io/gitlab/pipeline/joaomarcoscarvalho/desafio-cubos" />
 <img src="https://img.shields.io/badge/💙-QueroSerCubico-blue">
<img src="https://badgen.net/badge/icon/vercel?icon=vercel&label" />
<p>

<p align="center">
 <a href="#instalacao">Instalação</a> • 
 <a href="#funcionalidades">Funcionalidades</a> • <a href="#tecnologias">Tecnologias</a> • 
 <a href="#autor">Autor</a>
</p>
</br>
</br>

<h1>✅ Pré-requisitos</h1>
<p>💻 <strong>Computador com acesso à internet</strong></p>
<p>📨 <strong>Algum navegador para ter acesso ao website</strong></p>
</br>
</br>

<h1 id="instalacao">📎 Instalação</h1>
<p>Antes de dar o start é necessário fazer um clone deste projeto em sua máquina. Feito isso, você irá digitar no seu terminal/Prompt dentro da pasta o comando <strong>yarn</strong> para que os pacotes necessários sejam instalados.<p>
<h3>Agora já pode dar um <strong>yarn start</strong> e ver a mágica acontecer</h3>
</br>
</br>

<h1 id="funcionalidades"> 🛠 Funcionalidade</h1>
</br>
</br>
<h2>Página principal</h2>
<ul>
  <li>
    Pesquisar Filmes por gênero, ano, nome
  </li>
  <li>Resultados paginados (5 por página e 5 páginas por pesquisa)</li>
</ul>
</br>
<img align="center" src="./src/readmeImages/ReactApp.png" />
</br>
</br>
<h2>Página detalhes</h2>
<ul>
  <li>Detalhes do filme, incluindo: orçamento, lucro, gastos, duração, idioma nativo, entre outros.</li>
  <li>Video contendo o trailer do filme.</li>
</ul>
</br>
<img src="./src/readmeImages/ReactAppDetails.png"/>
</br>
</br>
<h1 id="tecnologias">💎 Tecnologias</h1>
<ul>
<li><h3>React<h3></li>
<li><h3>Redux<h3></li>
<li><h3>Redux Thunk<h3></li>
<li><h3>Styled-components<h3></li>
<li><h3>Axios<h3></li>
</ul>
</br>
</br>

<h1 id="autor">👨🏿 Autor<h1>

<a href="https://www.instagram.com/_marcarjao/">
 <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/57837180?s=460&u=440ff8f5ac5edf181ab12bc8435083dd27920e7a&v=4" width="100px;" alt=""/>
 <br />
 <sub><b>João Marcos</b></sub></a> <a href="https://www.instagram.com/_marcarjao/" title="Instagram">🚀</a>

Feito com ❤️ por João Marcos 👋🏽

[![Linkedin Badge](https://img.shields.io/badge/-João-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/joaomarcosc/)](https://www.linkedin.com/in/joaomarcosc/)
