import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import getMoviesReducer from "./modules/movies/reducer";

const reducer = combineReducers({
  movies: getMoviesReducer,
});

const store = createStore(reducer, applyMiddleware(thunk));

export default store;
