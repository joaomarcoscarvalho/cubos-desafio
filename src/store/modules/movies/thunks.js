import { api } from "../../../services/api";
import { getGenres, getMovies, getSearch, getDetails } from "./actions";

// Movie List
export const getMoviesThunk = (page) => (dispatch, getState) => {
  const { movies } = getState();
  api
    .get("search/movie/", {
      params: {
        query: movies.search === "" ? "marvel" : movies.search,
        year: movies.search,
        page: page || 1,
      },
    })
    .then((res) => {
      const movieListFive = res.data.results.slice(0, 5);
      dispatch(
        getMovies({
          movieList: movieListFive,
          pages: res.data.total_pages,
        })
      );
    });
};

// Search Component
export const getSearchThunk = (name) => (dispatch) => {
  dispatch(getSearch(name));
};

// Genres
export const getGenresThunk = () => (dispatch) => {
  api.get("genre/movie/list").then((res) => {
    dispatch(getGenres(res.data.genres));
  });
};

// Details
export const getDetailsThunk = (id) => (dispatch) => {
  api
    .get(`/movie/${id}`, {
      params: {
        append_to_response: "videos",
      },
    })
    .then((res) => dispatch(getDetails(res.data)));
};
