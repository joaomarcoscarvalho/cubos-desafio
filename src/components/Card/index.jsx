import { ListItem } from "./styles";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

const Card = ({
  title,
  poster_path,
  overview,
  vote_average,
  release_date,
  id,
  genre_ids,
  index,
}) => {
  const genres = useSelector((state) => state.movies.genres);
  const history = useHistory();
  const imageUrl = "https://image.tmdb.org/t/p/w500";
  return (
    <ListItem key={index}>
      <img src={`${imageUrl}${poster_path}`} alt={title} />
      <div className="details">
        <div
          className="title_popularity"
          onClick={() => history.push(`/details/${id}`)}
        >
          <div className="popularity">
            <p>{parseInt(vote_average * 10)}%</p>
          </div>
          <h2>{title}</h2>
        </div>
        <p className="release_date">
          {release_date?.split("-").reverse().join("/")}
        </p>
        <p className="overview">{overview}</p>
        <div className="genres">
          {genres?.map(
            (genre, index) =>
              genre_ids?.indexOf(genre.id) > -1 && (
                <p key={index}>{genre.name}</p>
              )
          )}
        </div>
      </div>
    </ListItem>
  );
};

export default Card;
