import styled from "styled-components";
import {
  LIGHT_BLUE,
  DARK_BLUE,
  ABEL,
  LATO,
} from "../../variables/variablesCss";

export const ListItem = styled.article`
  display: flex;
  flex-direction: column;
  background-color: #ebebeb;
  margin-top: 5%;
  max-width: 100%;

  .title_popularity {
    display: flex;
    margin-bottom: 1%;
    background-color: ${DARK_BLUE};
    padding-top: 4%;
    padding-bottom: 0.5%;
    position: relative;
    cursor: pointer;

    h2 {
      margin-left: 51px;
      color: ${LIGHT_BLUE};
      font-family: ${ABEL}, sans-serif;
      font-weight: 300;
    }
    .popularity {
      width: 50px;
      height: 50px;
      background-color: ${DARK_BLUE};
      border-radius: 50%;
      display: flex;
      justify-content: center;
      align-items: center;
      position: absolute;
      left: 0;
      bottom: -20px;
      color: ${LIGHT_BLUE};
      font-family: ${ABEL}, sans-serif;

      p {
        width: 46px;
        height: 46px;
        display: flex;
        justify-content: center;
        align-items: center;
        border: 3px solid ${LIGHT_BLUE};
        border-radius: 50%;
      }
    }
  }

  img {
    width: 100%;
  }

  .release_date {
    margin-bottom: 4%;
    margin-left: 51px;
    font-family: ${ABEL}, sans-serif;
    font-size: 1.3rem;
    color: #b1b1b1;
  }
  .overview {
    font-family: ${LATO}, sans-serif;
    margin-bottom: 3%;
    margin-right: 4%;
    margin-left: 4%;
    text-align: justify;
    color: #7a7a7a;
  }

  .genres {
    display: flex;
    flex-wrap: wrap;
    margin-left: 4%;
  }

  .genres > p {
    padding: 4px 12px;
    border: 2px solid ${DARK_BLUE};
    border-radius: 20px;
    font-family: ${ABEL}, sans-serif;
    color: ${DARK_BLUE};
    background-color: white;
    margin-right: 2%;
    margin-bottom: 2%;
  }

  @media (min-width: 769px) {
    flex-direction: row;
    margin-top: 5%;
    .title_popularity {
      .popularity {
        width: 70px;
        height: 70px;
        margin-left: 20px;
        bottom: -32.5px;

        p {
          width: 66px;
          height: 66px;
          font-size: 1.2rem;
          border: 5px solid ${LIGHT_BLUE};
        }
      }
      h2 {
        font-size: 2rem;
        margin-left: 96px;
      }
    }

    .release_date {
      margin-left: 96px;
    }

    img {
      width: 250px;
    }

    .details {
      width: 100%;
    }
  }

  @media (max-width: 768px) and (min-width: 767px) {
    .overview {
      font-size: 1.6rem;
    }
    .title_popularity {
      font-size: 1.6rem;
    }
    .genres > p {
      font-size: 1.4rem;
    }
    .title_popularity {
      .popularity {
        width: 80px;
        height: 80px;

        p {
          width: 66px;
          height: 66px;
        }
      }
      h2 {
        font-size: 2rem;
        margin-left: 100px;
      }
    }
    .release_date {
      margin-left: 100px;
    }
  }

  @media (min-width: 1300px) {
    width: 100%;
  }
`;
