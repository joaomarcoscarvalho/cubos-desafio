import styled from "styled-components";
import { ABEL, LIGHT_BLUE, DARK_BLUE } from "../../variables/variablesCss";

export const MainHeader = styled.header`
  width: 85vw;
  max-width: 1400vw;
  height: 15vh;
  max-height: 100px;
  background-color: ${DARK_BLUE};
  display: flex;
  align-items: center;
  justify-content: center;

  h1 {
    font-family: ${ABEL}, sans-serif;
    font-weight: 100;
    font-size: 2.5rem;
    letter-spacing: 1px;
    color: ${LIGHT_BLUE};
  }
`;
