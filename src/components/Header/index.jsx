import { MainHeader } from "./styles";

const Header = () => {
  return (
    <MainHeader>
      <h1>Movies</h1>
    </MainHeader>
  );
};

export default Header;
