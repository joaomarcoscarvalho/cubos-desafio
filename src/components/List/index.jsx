import { useSelector } from "react-redux";
import Card from "../Card";

const List = () => {
  const movies = useSelector((state) => state.movies.movies.movieList);

  return (
    <>
      {movies?.map(
        (
          {
            title,
            poster_path,
            overview,
            vote_average,
            release_date,
            id,
            genre_ids,
          },
          index
        ) => (
          <Card
            title={title}
            poster_path={poster_path}
            overview={overview}
            vote_average={vote_average}
            release_date={release_date}
            id={id}
            genre_ids={genre_ids}
            index={index}
          />
        )
      )}
    </>
  );
};
export default List;
