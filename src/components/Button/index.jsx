import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { ContainerButtons, Btn } from "./styles";

const Button = ({ page }) => {
  const buttonsSize = useSelector((state) => state.movies.movies.pages);
  const [active, setActive] = useState(parseInt(page));
  const history = useHistory();

  const changingPage = (i) => {
    history.push(`/${i}`);
    setActive(i);
  };

  useEffect(() => {
    setActive(parseInt(page) || 1);
  }, [page]);

  const buttonsGroup = [];
  const limit = buttonsSize > 5 ? 5 : buttonsSize;

  for (let i = 1; i <= limit; i++) {
    buttonsGroup.push(
      <Btn
        className="button_div"
        active={active === i}
        onClick={() => changingPage(i)}
        key={i}
      >
        <p className="button_div_number"></p>
        {i}
      </Btn>
    );
  }
  return (
    <ContainerButtons>{buttonsGroup.map((button) => button)}</ContainerButtons>
  );
};

export default Button;
