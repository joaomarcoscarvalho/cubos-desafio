import styled, { css } from "styled-components";
import { LIGHT_BLUE, DARK_BLUE, ABEL } from "../../variables/variablesCss";

export const ContainerButtons = styled.section`
  width: 30vw;
  max-width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 2rem;
  @media (max-width: 481px) {
    width: 80vw;
    margin-bottom: 2rem;
  }

  @media (max-width: 1024px) {
    width: 80vw;
  }
`;

export const Btn = styled.div`
  font-family: ${ABEL}, sans-serif;
  margin: 4%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 2.5rem;
  cursor: pointer;
  color: ${DARK_BLUE};
  ${({ active }) =>
    active &&
    css`
      margin-left: 5%;
      margin-right: 5%;
      width: 80px;
      height: 80px;
      border-radius: 50%;
      background-color: ${DARK_BLUE};
      position: relative;
      color: ${LIGHT_BLUE};
      .button_div_number {
        width: 90%;
        height: 90%;
        display: flex;
        position: absolute;
        justify-content: center;
        align-items: center;
        border: 4px solid ${LIGHT_BLUE};
        border-radius: 50%;
      }
    `};

  @media (max-width: 481px) {
    font-size: 1.5rem;
  }
`;
