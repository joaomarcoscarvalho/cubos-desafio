import {
  getSearchThunk,
  getMoviesThunk,
} from "../../store/modules/movies/thunks";
import { useDispatch, useSelector } from "react-redux";
import { InputBar } from "./styles";
import { useHistory } from "react-router-dom";
import { useEffect } from "react";

const SearchBar = ({ page }) => {
  const search = useSelector((state) => state.movies.search);
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMoviesThunk(parseInt(page)));
    history.push(parseInt(page));
  }, [search]);

  const handleChange = (e) => {
    dispatch(getSearchThunk(e.target.value));
    history.push("/1");
  };

  return (
    <InputBar>
      <input
        value={search}
        type="text"
        className="Input"
        placeholder="Busque um filme por ano, nome ou gênero..."
        onChange={handleChange}
      />
    </InputBar>
  );
};

export default SearchBar;
