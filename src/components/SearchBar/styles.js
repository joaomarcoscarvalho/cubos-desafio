import styled from "styled-components";
import { DARK_BLUE, ABEL } from "../../variables/variablesCss";

export const InputBar = styled.section`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 5%;
  height: 10vh;
  max-height: 100px;
  .Input {
    width: 100%;
    height: 100%;
    background-color: #ebebeb;
    color: ${DARK_BLUE};
    border: none;
    border-radius: 40px;
    padding-left: 15px;
    font-size: 2rem;
    outline: none;
    font-family: ${ABEL}, sans-serif;
  }

  input::placeholder {
    color: ${DARK_BLUE};
    opacity: 0.5;
  }

  @media (max-width: 1024px) and (min-width: 767px) {
    height: 6vh;
  }
  input {
    font-size: 1.4rem;
  }

  @media (max-width: 768px) and (min-width: 426px) {
    height: 6vh;
  }
  input {
    font-size: 1.4rem;
  }

  @media (max-width: 425px) {
    height: 8vh;
    .Input {
      font-size: 0.8rem;
    }
  }
`;
