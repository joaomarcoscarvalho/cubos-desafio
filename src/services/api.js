import axios from "axios";
import { API_KEY } from "./API_KEY";

export const api = axios.create({
  baseURL: "https://api.themoviedb.org/3/",
  params: {
    api_key: API_KEY,
    language: "pt-BR",
    include_image_language: "pt-BR",
  },
  https: {
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "text/plain",
    },
  },
});
