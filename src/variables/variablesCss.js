export const LIGHT_BLUE = "#00dede";
export const DARK_BLUE = "#116193";
export const BG_DETAILS = "#f2f2f2";
export const BG_TITLE_DETAILS = " #e6e6e6";
export const COLOR_FONT_TITLE_DETAILS = "#b0b0b0";
export const LATO = "Lato";
export const ABEL = "Abel";
