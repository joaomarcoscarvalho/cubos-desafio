import { Switch, Route } from "react-router-dom";
import Details from "../pages/Details";
import Home from "../pages/Home";
const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/:page" component={Home} />
      <Route path="/details/:id" component={Details} />
    </Switch>
  );
};
export default Routes;
