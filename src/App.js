import { GlobalStyle, Container } from "./styles";
import Routes from "./Routes";
import Header from "./components/Header";

function App() {
  return (
    <Container>
      <GlobalStyle />
      <Header />
      <Routes />
    </Container>
  );
}

export default App;
