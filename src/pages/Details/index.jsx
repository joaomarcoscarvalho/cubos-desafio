import { useParams } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getDetailsThunk } from "../../store/modules/movies/thunks";
import {
  Container,
  CardDetails,
  InfoContainer,
  TopTitle,
  Content,
  InfoData,
  GenresContent,
  PopularityContainer,
  Popularity,
  BorderPopularity,
} from "./styles";

import "@formatjs/intl-displaynames/polyfill";
import "@formatjs/intl-displaynames/locale-data/pt";

const Details = () => {
  const { id } = useParams();
  const details = useSelector((state) => state.movies.details);
  const imageUrl = "https://image.tmdb.org/t/p/w300";

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDetailsThunk(id));
  }, []);

  const convertTime = (time) => {
    const hours = Math.floor(time / 60);
    const min = time % 60;
    const res = `${hours}h ${min}min`;
    return res;
  };

  const languageName = new Intl.DisplayNames(["pt-BR"], {
    type: "language",
  });

  let lang = details.original_language || "pt";
  lang = languageName.of(lang);

  return (
    <Container>
      <TopTitle>
        <h2>{details.title}</h2>
        <p>{details.release_date?.split("-").reverse().join("/")}</p>
      </TopTitle>
      <CardDetails>
        <InfoContainer>
          <p className="subtitle">Sinopse</p>
          <p className="overview">{details.overview}</p>
          <p className="subtitle">Informações</p>
          <InfoData>
            <Content>
              <p className="subtitleData">Situação</p>
              <p>{details.status === "Released" ? "Lançado" : "Planejado"}</p>
            </Content>
            <Content>
              <p className="subtitleData">Idioma</p>
              <p>{lang}</p>
            </Content>
            <Content>
              <p className="subtitleData">Duração</p>
              <p>{convertTime(details.runtime)}</p>
            </Content>
            <Content>
              <p className="subtitleData">Orçamento</p>
              <p>
                {details.budget?.toLocaleString("en-US", {
                  style: "currency",
                  currency: "USD",
                })}
              </p>
            </Content>
            <Content>
              <p className="subtitleData">Receita</p>
              <p>
                {details.revenue?.toLocaleString("en-US", {
                  style: "currency",
                  currency: "USD",
                })}
              </p>
            </Content>
            <Content>
              <p className="subtitleData">Lucro</p>
              <p>
                {(details.revenue - details.budget).toLocaleString("en-US", {
                  style: "currency",
                  currency: "USD",
                })}
              </p>
            </Content>
          </InfoData>
          <GenresContent>
            {details.genres?.map(({ name }, index) => (
              <p key={index} className="genres">
                {name}
              </p>
            ))}
          </GenresContent>
        </InfoContainer>
        <img src={`${imageUrl}${details.poster_path}`} alt={details.title} />
        <PopularityContainer>
          <Popularity>
            {parseInt(details.vote_average * 10)}%
            <BorderPopularity />
          </Popularity>
        </PopularityContainer>
      </CardDetails>
      {details.videos?.results?.length > 0 && (
        <iframe
          width="100%"
          height="500"
          src={`https://www.youtube.com/embed/${details.videos.results[0].key}?autoplay=1`}
          frameBorder="0"
          title={details.title}
        ></iframe>
      )}
    </Container>
  );
};

export default Details;
