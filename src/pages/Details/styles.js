import styled from "styled-components";
import {
  ABEL,
  LATO,
  LIGHT_BLUE,
  DARK_BLUE,
} from "../../variables/variablesCss";

export const Container = styled.section`
  width: 85vw;
  max-width: 1400px;
  margin-top: 3%;
  margin-bottom: 6%;
  div {
    margin-bottom: 3%;
    img {
      width: 100%;
      height: 100%;
    }

    @media (min-width: 700px) {
      display: flex;
      justify-content: space-between;
      img {
        width: 300px;
      }
    }
  }

  p,
  h2 {
    font-family: ${ABEL}, sans-serif;
  }
`;

export const CardDetails = styled.div`
  position: relative;
`;

export const TopTitle = styled.section`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #e6e6e6;
  padding: 20px 3%;

  h2 {
    color: ${DARK_BLUE};
    font-size: 2rem;
    font-weight: 200;
  }

  p {
    color: #b0b0b0;
    font-size: 1.2rem;
  }
`;

export const InfoContainer = styled.section`
  padding: 0 3%;
  background-color: #f2f2f2;
  @media (min-width: 700px) {
    width: 90%;
  }
  .subtitle {
    font-size: 1.5rem;
    border-bottom: 2px solid ${LIGHT_BLUE};
    padding-top: 3%;
    margin-bottom: 2%;
    padding-bottom: 0.2rem;
    color: ${DARK_BLUE};
  }

  .overview {
    font-family: ${LATO}, sans-serif;
    font-size: 0.9rem;
    color: #636363;
  }
`;

export const InfoData = styled.section`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

export const Content = styled.section`
  text-align: center;
  margin-bottom: 3%;
  text-transform: capitalize;

  .subtitleData {
    font-size: 1.3rem;
    font-family: ${ABEL}, sans-serif;
    color: ${DARK_BLUE};
  }

  p {
    font-family: ${LATO}, sans-serif;
    font-size: 0.9rem;
    color: #636363;
  }
`;

export const GenresContent = styled.section`
  display: flex;
  flex-wrap: wrap;
  padding-bottom: 3%;
  p {
    padding: 4px 12px;
    border: 2px solid ${DARK_BLUE};
    border-radius: 20px;
    font-family: ${ABEL}, sans-serif;
    color: ${DARK_BLUE};
    background-color: white;
    margin-right: 2%;
  }
`;

export const PopularityContainer = styled.section`
  background-color: ${DARK_BLUE};
  position: absolute;
  bottom: 5%;
  right: 30%;
  width: 100px;
  height: 100px;
  border-radius: 50%;

  @media (max-width: 1024px) {
    right: 35%;
  }

  @media (max-width: 768px) {
    bottom: -10px;
    right: 34%;
  }

  @media (max-width: 425px) {
    right: 0;
    bottom: 46%;
  }

  @media (max-width: 375px) {
    bottom: 41%;
  }

  @media (max-width: 320px) {
    bottom: 35%;
  }
`;

export const Popularity = styled.section`
  position: relative;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 2rem;
  color: ${LIGHT_BLUE};
`;

export const BorderPopularity = styled.section`
  position: absolute;
  width: 90%;
  height: 90%;
  border-radius: 50%;
  border: 6px solid ${LIGHT_BLUE};
`;
