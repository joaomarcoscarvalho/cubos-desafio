import List from "../../components/List";
import Button from "../../components/Button";
import SearchBar from "../../components/SearchBar";
import { Container } from "./styles";
import { useParams, useHistory } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import {
  getMoviesThunk,
  getGenresThunk,
} from "../../store/modules/movies/thunks";
const Home = () => {
  const { page } = useParams();
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMoviesThunk(parseInt(page)));
    dispatch(getGenresThunk());
    history.push(parseInt(page));
  }, []);

  useEffect(() => {
    dispatch(getMoviesThunk(parseInt(page)));
  }, [page]);

  return (
    <>
      <Container>
        <SearchBar page={page} />
        <List />
      </Container>
      <Button page={page} />
    </>
  );
};

export default Home;
